---
layout:     post
title:      x64dbg python API
date:       2018-12-20
top_img: http://imgset.gitee.io/img/1.jpg
categories:
    - Note
tags:
    - x64dbg

typora-copy-images-to: ../img
typora-root-url: ..
---

1

<https://github.com/x64dbg/x64dbgpy>

asdfasfd

NAME
# x64dbgpy.pluginsdk._scriptapi

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\__init__.py

PACKAGE CONTENTS
​    argument
​    assembler
​    bookmark
​    comment
​    debug
​    flag
​    function
​    gui
​    label
​    memory
​    misc
​    module
​    pattern
​    register
​    stack
​    symbol

DATA
​    GUI_MAX_LINE_SIZE = 65536
​    MAX_COMMENT_SIZE = 512
​    MAX_ERROR_SIZE = 512
​    MAX_LABEL_SIZE = 256
​    MAX_MODULE_SIZE = 256
​    MAX_PATH = 260
​    MAX_STRING = 512



---------------------


NAME
# x64dbgpy.pluginsdk._scriptapi.argument

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\argument.py

FUNCTIONS
​    Add(start, end, manual, instructionCount=0)
​    
​    AddInfo(info)
​    
​    Clear()
​    
​    Delete(addr)
​    
​    DeleteRange(start, end, deleteManual=False)
​    
​    Get(addr)
​    
​    GetInfo(addr)
​    
​    GetList()
​    
​    Overlaps(start, end)

-------------------


NAME
#  x64dbgpy.pluginsdk._scriptapi.assembler

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\assembler.py

FUNCTIONS
​    Assemble(addr, instruction)
​    
​    AssembleEx(addr, instruction)
​    
​    AssembleMem(addr, instruction)
​    
​    AssembleMemEx(addr, instruction, fillnop)

DATA
​    MAX_ERROR_SIZE = 512
​    MAX_STRING = 512

----------


NAME
# x64dbgpy.pluginsdk._scriptapi.bookmark

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\bookmark.py

FUNCTIONS
​    Clear()
​    
​    Delete(addr)
​    
​    DeleteRange(start, end)
​    
​    Get(addr)
​    
​    GetInfo(addr)
​    
​    GetList()
​    
​    Set(addr, manual=False)
​    
​    SetInfo(info)

---------

NAME
# x64dbgpy.pluginsdk._scriptapi.comment

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\comment.py

FUNCTIONS
​    Clear()
​    
​    Delete(addr)
​    
​    DeleteRange(start, end)
​    
​    Get(addr)
​    
​    GetInfo(addr)
​    
​    GetList()
​    
​    Set(addr, text, manual=False)
​    
​    SetInfo(info)

DATA
​    MAX_COMMENT_SIZE = 512

------------

NAME

# x64dbgpy.pluginsdk._scriptapi.debug

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\debug.py

CLASSES
​    HardwareType
​    
​    class HardwareType
​     |  Data and other attributes defined here:
​     |  
​     |  HardwareAccess = 0
​     |  
​     |  HardwareExecute = 2
​     |  
​     |  HardwareWrite = 1

FUNCTIONS
​    DeleteBreakpoint(address)
​    
​    DeleteHardwareBreakpoint(address)
​    
​    Run()
​    
​    SetBreakpoint(address)
​    
​    SetHardwareBreakpoint(address, type=2)
​    
​    StepIn()
​    
​    StepOut()
​    
​    StepOver()
​    
​    Stop()
​    
​    Wait()

---------------------

NAME
# x64dbgpy.pluginsdk._scriptapi.flag

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\flag.py

CLASSES
​    FlagEnum
​    
​    class FlagEnum
​     |  Data and other attributes defined here:
​     |  
​     |  AF = 6
​     |  
​     |  CF = 2
​     |  
​     |  DF = 7
​     |  
​     |  IF = 8
​     |  
​     |  OF = 1
​     |  
​     |  PF = 3
​     |  
​     |  SF = 4
​     |  
​     |  TF = 5
​     |  
​     |  ZF = 0

FUNCTIONS
​    Flag_Get(flag)
​    
​    Flag_Set(flag, value)
​    
​    GetAF()
​    
​    GetCF()
​    
​    GetDF()
​    
​    GetIF()
​    
​    GetOF()
​    
​    GetPF()
​    
​    GetSF()
​    
​    GetTF()
​    
​    GetZF()
​    
​    SetAF(value)
​    
​    SetCF(value)
​    
​    SetDF(value)
​    
​    SetIF(value)
​    
​    SetOF(value)
​    
​    SetPF(value)
​    
​    SetSF(value)
​    
​    SetTF(value)
​    
​    SetZF(value)

--------------


NAME
#  x64dbgpy.pluginsdk._scriptapi.function

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\function.py

FUNCTIONS
​    Add(start, end, manual, instructionCount=0)
​    
​    AddInfo(info)
​    
​    Clear()
​    
​    Delete(addr)
​    
​    DeleteRange(start, end, deleteManual=False)
​    
​    Get(addr)
​    
​    GetInfo(addr)
​    
​    GetList()
​    
​    Overlaps(start, end)

----------------

NAME
# x64dbgpy.pluginsdk._scriptapi.gui

FILE
    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\gui.py

FUNCTIONS
    Disassembly_SelectionGet()
    Script::Gui::Disassembly

    Disassembly_SelectionGetEnd()
    
    Disassembly_SelectionGetStart()
    
    Disassembly_SelectionSet(start, end)
    
    Dump_SelectionGet()
    Script::Gui::Dump

    Dump_SelectionGetEnd()
    
    Dump_SelectionGetStart()
    
    Dump_SelectionSet(start, end)
    
    Gui_SelectionGet(window)
   Script::Gui

    Gui_SelectionGetEnd(window)
    
    Gui_SelectionGetStart(window)
    
    Gui_SelectionSet(window, start, end)
    
    InputLine(title)
    
    InputValue(title)
    
    Message(message)
    
    MessageYesNo(message)
    
    Refresh()
    
    Stack_SelectionGet()
    Script::Gui::Stack

    Stack_SelectionGetEnd()
    
    Stack_SelectionGetStart()
    
    Stack_SelectionSet(start, end)

DATA
    GUI_MAX_LINE_SIZE = 65536

-----


NAME
# x64dbgpy.pluginsdk._scriptapi.label

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\label.py

FUNCTIONS
​    Clear()
​    
​    Delete(addr)
​    
​    DeleteRange(start, end)
​    
​    FromString(label)
​    
​    Get(addr)
​    
​    GetInfo(addr)
​    
​    GetList()
​    
​    Set(addr, text, manual=False)
​    
​    SetInfo(info)

DATA
​    MAX_LABEL_SIZE = 256

-----


NAME
# x64dbgpy.pluginsdk._scriptapi.memory

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\memory.py

FUNCTIONS
​    IsValidPtr(addr)
​    
​    Read(addr, size)
​    
​    ReadByte(addr)
​    
​    ReadDword(addr)
​    
​    ReadPtr(addr)
​    
​    ReadWord(addr)
​    
​    RemoteAlloc(size, addr=0)
​    
​    RemoteFree(addr)
​    
​    Write(addr, data)
​    
​    WriteByte(addr, data)
​    
​    WriteDword(addr, data)
​    
​    WritePtr(addr, data)
​    
​    WriteWord(addr, data)

---------


# x64dbgpy.pluginsdk._scriptapi.misc

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\misc.py

FUNCTIONS
​    Alloc(size)
​    
​    Free(ptr)
​    
​    ParseExpression(expression)
​    
​    RemoteGetProcAddress(module, api)
​    
​    ResolveLabel(label)

-----

NAME
# x64dbgpy.pluginsdk._scriptapi.module

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\module.py

FUNCTIONS
​    BaseFromAddr(addr)
​    
​    BaseFromName(name)
​    
​    EntryFromAddr(addr)
​    
​    EntryFromName(name)
​    
​    GetList()
​    
​    GetMainModuleBase()
​    
​    GetMainModuleEntry()
​    
​    GetMainModuleInfo()
​    
​    GetMainModuleName()
​    
​    GetMainModulePath()
​    
​    GetMainModuleSectionCount()
​    
​    GetMainModuleSectionList()
​    
​    GetMainModuleSize()
​    
​    InfoFromAddr(addr)
​    
​    InfoFromName(name)
​    
​    NameFromAddr(addr)
​    
​    PathFromAddr(addr)
​    
​    PathFromName(name)
​    
​    SectionCountFromAddr(addr)
​    
​    SectionCountFromName(name)
​    
​    SectionFromAddr(addr, number)
​    
​    SectionFromName(name, number)
​    
​    SectionListFromAddr(addr)
​    
​    SectionListFromName(name)
​    
​    SizeFromAddr(addr)
​    
​    SizeFromName(name)

DATA
​    MAX_MODULE_SIZE = 256
​    MAX_PATH = 260

-----

NAME
# x64dbgpy.pluginsdk._scriptapi.pattern

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\pattern.py

FUNCTIONS
​    Find(data, pattern)
​    
​    FindMem(start, size, pattern)
​    
​    SearchAndReplace(data, searchpattern, replacepattern)
​    
​    SearchAndReplaceMem(start, size, searchpattern, replacepattern)
​    
​    Write(data, pattern)
​    
​    WriteMem(start, size, pattern)

----

NAME

# x64dbgpy.pluginsdk._scriptapi.register

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\register.py

FUNCTIONS​    GetAH()
​    
​    GetAL()
​    
​    GetAX()
​    
​    GetBH()
​    
​    GetBL()
​    
​    GetBP()
​    
​    GetBX()
​    
​    GetCH()
​    
​    GetCIP()
​        \# Generic Registers
​    
​    GetCL()
​    
​    GetCSP()
​    
​    GetCX()
​    
​    GetDH()
​    
​    GetDI()
​    
​    GetDL()
​    
​    GetDR0()
​        \# x86 Debug Registers
​    
​    GetDR1()
​    
​    GetDR2()
​    
​    GetDR3()
​    
​    GetDR6()
​    
​    GetDR7()
​    
​    GetDX()
​    
​    GetEAX()
​        \# x86 Registers
​    
​    GetEBP()
​    
​    GetEBX()
​    
​    GetECX()
​    
​    GetEDI()
​    
​    GetEDX()
​    
​    GetEIP()
​    
​    GetESI()
​    
​    GetESP()
​    
​    GetSI()
​    
​    GetSP()
​    
​    SetAH(value)
​    
​    SetAL(value)
​    
​    SetAX(value)
​    
​    SetBH(value)
​    
​    SetBL(value)
​    
​    SetBP(value)
​    
​    SetBX(value)
​    
​    SetCH(value)
​    
​    SetCIP(value)
​    
​    SetCL(value)
​    
​    SetCSP(value)
​    
​    SetCX(value)
​    
​    SetDH(value)
​    
​    SetDI(value)
​    
​    SetDL(value)
​    
​    SetDR0(value)
​    
​    SetDR1(value)
​    
​    SetDR2(value)
​    
​    SetDR3(value)
​    
​    SetDR6(value)
​    
​    SetDR7(value)
​    
​    SetDX(value)
​    
​    SetEAX(value)
​    
​    SetEBP(value)
​    
​    SetEBX(value)
​    
​    SetECX(value)
​    
​    SetEDI(value)
​    
​    SetEDX(value)
​    
​    SetEIP(value)
​    
​    SetESI(value)
​    
​    SetESP(value)
​    
​    SetSI(value)
​    
​    SetSP(value)
​    
​    Size()

-----


NAME
# x64dbgpy.pluginsdk._scriptapi.stack

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\stack.py

FUNCTIONS
​    Peek(offset=0)
​    
​    Pop()
​    
​    Push(value)

------


NAME
# x64dbgpy.pluginsdk._scriptapi.symbol

FILE
​    c:\softfo~1\release\x32\plugins\x64dbgpy\x64dbgpy\pluginsdk\_scriptapi\symbol.py

CLASSES
​    SymbolType
​    
​    class SymbolType
​     |  Data and other attributes defined here:
​     |  
​     |  Export = 2
​     |  
​     |  Function = 0
​     |  
​     |  Import = 1

FUNCTIONS
​    GetList()
