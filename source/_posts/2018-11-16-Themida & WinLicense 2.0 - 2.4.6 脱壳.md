---
layout:     post
title:      Themida & WinLicense 2.0 - 2.4.6 脱壳
date:       2018-11-16
top_img: http://imgset.gitee.io/img/windows_keyboard.jpg
categories:
    - Reverse
tags:



typora-copy-images-to: ../img
typora-root-url: ..

---





碰上了这个壳，具体文件就不说了

百度查了一圈找不到相关文章？难道要手脱？

濒临绝望之前，看到了国外某大佬的文章，链接：<https://zenhax.com/viewtopic.php?f=4&t=1051Hello>

来看过程吧



PEID查壳

![1571830112209](http://imgset.gitee.io/img/1571830112209.png)

脱壳需要的文件如下：

OD

插件：

ODBGScript v1.82.6
StrongOD 0.4.8.892
PhantOm 1.79
ARImpRec.dll

脱壳脚本（Themida - Winlicense Ultra Unpacker 1.4）



文件会在文末留下链接



在使用脱壳脚本之前，需要修改一下ARImpRec.dll的路径。

打开脚本文件搜索HERE_ENTER_YOUR_DLL_PATH_TO_ARIMPREC_DLL:

![1571830437147](http://imgset.gitee.io/img/1571830437147.png)

在324行中修改为你存储此dll的绝对路径。



还有一点需要注意的就是，要使用英文版本的OD，下图中的OllyDBG.exe，然后备份OllyDBG.ini文件。

并创建一个新的OllyDBG.ini，使其文件内容为空

![1571830775825](http://imgset.gitee.io/img/1571830775825.png)



打开OllDBG.exe，加载待脱壳文件，然后加载脚本

![1571831000405](http://imgset.gitee.io/img/1571831000405.png)



运行脚本

![1571831031397](http://imgset.gitee.io/img/1571831031397.png)

点击是

![1571831063517](http://imgset.gitee.io/img/1571831063517.png)

点击否

![1571831111781](http://imgset.gitee.io/img/1571831111781.png)

脚本已经开始工作了

![1571831144245](http://imgset.gitee.io/img/1571831144245.png)

在运行几秒后，脚本暂停在了上图的位置，继续运行脚本即可

![1571831251717](http://imgset.gitee.io/img/1571831251717.png)

随后我们获得了如下图的弹窗，需要在OllyDBG.ini文件中，根据提示修改文件，并重新运行脚本，我在文末的打包文件中已经修改好了

![1571831293135](http://imgset.gitee.io/img/1571831293135.png)

关掉弹窗，继续运行

![1571831409264](http://imgset.gitee.io/img/1571831409264.png)

点击是，进行更多的检查

![1571831485640](http://imgset.gitee.io/img/1571831485640.png)

在这一步，点击否

![1571832082751](http://imgset.gitee.io/img/1571832082751.png)

如果你在虚拟机中运行点击时，真机的话点击否就好了

![1571832196873](http://imgset.gitee.io/img/1571832196873.png)

搞定了，现在可以进行dump了，点击是

![1571832266301](http://imgset.gitee.io/img/1571832266301.png)

按照上图说的去做，第一次看到这个弹窗，点击否，不是第一次点击是

![1571832353478](http://imgset.gitee.io/img/1571832353478.png)

文件大小可以接受，不需要压缩，点击是

![1571832437892](http://imgset.gitee.io/img/1571832437892.png)、

一些文件信息，点击是

OK，脱壳结束，脱壳的文件被命名为<原文件名>_DP。

再来查一下壳

![1571832578604](http://imgset.gitee.io/img/1571832578604.png)

搞定。

































Themida & WinLicense 2.0 - 2.4.6 
