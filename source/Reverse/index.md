---
title: 实例
date: 2019-12-16 22:09:42
type: "Reverse"
---



[网易xx游戏辅助的心路历程](https://basicbit.cn/2018/11/20/2018-11-19-%E7%BD%91%E6%98%93%E6%B8%B8%E6%88%8F%E8%BE%85%E5%8A%A9%E7%9A%84%E5%BF%83%E8%B7%AF%E5%8E%86%E7%A8%8B/)
[反调试总结](https://basicbit.cn/2018/11/30/2018-11-30-%E5%8F%8D%E8%B0%83%E8%AF%95%E6%80%BB%E7%BB%93/)
[网易游戏的四种解密&解包，附工具](https://basicbit.cn/2019/03/01/2019-03-01-网易游戏的四种解密&解包，附工具/)
[vmp3.3.1 虚拟机分析](https://basicbit.cn/2019/01/01/2018-11-03-vmp3.3.1%20虚拟机分析/)
[vmp 跳过代码校验](https://basicbit.cn/2018/12/31/2018-12-30-vmp%20%E8%BF%87%E4%BB%A3%E7%A0%81%E6%A0%A1%E9%AA%8C/)
[python  pyc文件结构](https://basicbit.cn/2018/12/20/2018-12-20-Python%E5%8F%8D%E7%BC%96%E8%AF%91%EF%BC%9F%E5%85%88%E8%81%8A%E8%81%8Apyc%E7%BB%93%E6%9E%84%E5%90%A7/)
[微信 消息结构分析](https://basicbit.cn/2018/11/16/2018-11-16-%E5%BE%AE%E4%BF%A1%E6%8E%A5%E5%8F%A3%E5%88%86%E6%9E%90/)
[微信 - 撤回分析 & patch](https://basicbit.cn/2018/11/16/2018-11-16-%E5%BE%AE%E4%BF%A1%E6%B6%88%E6%81%AF%E6%92%A4%E5%9B%9E%E5%88%86%E6%9E%90/)
[Themida & WinLicense 2.0 - 2.4.6 脱壳](https://basicbit.cn/2018/11/16/2018-11-16-Themida%20&%20WinLicense%202.0%20-%202.4.6%20%E8%84%B1%E5%A3%B3/)
[10分钟破解收银软件](https://basicbit.cn/2018/11/12/2018-11-14-10%E5%88%86%E9%92%9F%E7%A0%B4%E8%A7%A3%E6%94%B6%E9%93%B6%E8%BD%AF%E4%BB%B6/)
[一个简单的CTF小DEMO](https://basicbit.cn/2018/11/01/2018-11-02%20%E4%B8%80%E4%B8%AA%E7%AE%80%E5%8D%95%E7%9A%84CTF%E5%B0%8Fdemo/)
[Window PE感染型木马分析](https://basicbit.cn/2017/07/14/2017-07-14-PE%E6%84%9F%E6%9F%93%E5%9E%8B%E6%9C%A8%E9%A9%AC%E6%B1%87%E7%BC%96%E4%BB%A3%E7%A0%81%E5%88%86%E6%9E%90/)


# 提升
[局部静态变量只能初始化一次实现原理](https://basicbit.cn/2018/11/12/2018-11-14-%E9%9D%99%E6%80%81%E5%8F%98%E9%87%8F/)
[C++基于SEH二次封装的异常处理 - 流程与识别](https://basicbit.cn/2018/11/12/2018-11-14-C++%E5%BC%82%E5%B8%B8%E5%A4%84%E7%90%86%E7%9A%84%E6%B5%81%E7%A8%8B%E4%B8%8E%E8%AF%86%E5%88%AB/)
[C++基于SEH二次封装的异常处理 - 数据结构篇](https://basicbit.cn/2018/11/12/2018-11-14-C++%E5%BC%82%E5%B8%B8%E5%A4%84%E7%90%86%E7%9A%84%E4%BA%8C%E6%AC%A1%E5%B0%81%E8%A3%85/)
[C++ 多态的实现](https://basicbit.cn/2018/11/11/2018-11-11-%E6%88%91%E4%BB%AC%E6%9D%A5%E8%81%8A%E8%81%8AC++%E5%A4%9A%E6%80%81%E5%90%A7%EF%BC%8C%E7%90%86%E8%A7%A3%E5%AE%83%EF%BC%8C%E5%B9%B6%E6%89%BE%E5%88%B0%E5%AE%83/)
[从继承的角度出发再探多态](https://basicbit.cn/2018/11/12/2018-11-12-%E4%BB%8E%E7%BB%A7%E6%89%BF%E7%9A%84%E8%A7%92%E5%BA%A6%E5%86%8D%E6%9D%A5%E8%81%8A%E8%81%8A%E5%A4%9A%E6%80%81%E5%90%A7/)
[多重继承？抽象类？C++的内存布局并不复杂](https://basicbit.cn/2018/11/12/2018-11-13-%E5%A4%9A%E9%87%8D%E7%BB%A7%E6%89%BF%EF%BC%8C%E6%8A%BD%E8%B1%A1%EF%BC%8C%E7%BB%88%E7%BB%93%E7%AF%87/)
[静态变量底层实现](https://basicbit.cn/2018/10/20/2018-10-20-%E9%9D%99%E6%80%81%E5%8F%98%E9%87%8F%E5%BA%95%E5%B1%82%E5%AE%9E%E7%8E%B0/)
[Inline Hook](https://basicbit.cn/2018/01/16/2018-11-18-Inline%20Hook/)

# PE结构
[初识PE](https://basicbit.cn/2018/10/31/2018-10-31-Windows%20PE%20%E5%88%9D%E8%AF%86/)
[文件头](https://basicbit.cn/2018/11/01/2018-11-01-Windows%20PE%20%E6%96%87%E4%BB%B6%E5%A4%B4/)
[区块](https://basicbit.cn/2018/11/01/2018-11-01-Windows%20PE%20%E5%8C%BA%E5%9D%97/)
[ 输入表](https://basicbit.cn/2018/11/01/2018-11-01-Windows%20PE%20%E8%BE%93%E5%85%A5%E8%A1%A8/)
[输出表 基址重定位](https://basicbit.cn/2018/11/01/2018-11-01-Windows%20PE%20%E8%BE%93%E5%87%BA%E8%A1%A8/)
[资源](https://basicbit.cn/2018/11/01/2018-11-01-Windows%20PE%20%E8%B5%84%E6%BA%90/)

# 异常
[Window SEH异常 -- 异常初识](https://basicbit.cn/2018/10/31/2018-10-31-Windows%20Seh%20%E5%88%9D%E8%AF%86/)
[Window SEH异常 -- 异常基础](https://basicbit.cn/2018/10/31/2018-10-31-Window%20SEH%20%E5%8E%9F%E7%90%86/#Window-SEH%E5%BC%82%E5%B8%B8-%E2%80%93-%E5%BC%82%E5%B8%B8%E5%9F%BA%E7%A1%80)
