---
layout:     post
title:      Linux开机过程
subtitle:   按下电源键，Linux系统怎么进行启动的
date:       2016-03-10
author:     BY
top_img: img/current_linux_desktop_.jpg
categories:
    - OS
tags:
    - Linux
typora-root-url: ..
---



# Linux 开机过程



基于centos 6

## 当你按下电源键时

首先按下电源键会进入BIOS

BIOS 进行查找磁盘可以启动的磁盘



## 怎么判断磁盘是可以启动的

查看磁盘的前512字节的第446字节，如果是 55AA 那么这块磁盘是可启动的，此块磁盘的前512字节被称之为MBR。



MBR由三个部分组成

1. 第1-446字节：调用操作系统的机器码。 
2. 第447-510字节：分区表（Partition table）。 
3. 第511-512字节：主引导记录签名（0x55和0xAA）。 



引导文件一般放在/boot/grub/文件夹里 



最后会运行第一个进程 init 并且是所有进程的父进程 



通过grub.conf可以看到启动的配置文件

![centos6](http://imgset.gitee.io/img/centos6.png)

## grub.conf  参数配置



>  default默认开机条目  
>

>  timeout 默认等待选择时间
>
>  splashimage开机界面配置
>
>  root 表示开机分区hd代表硬盘0代表那块硬盘，1代表此块硬盘的那块分区
>
>  kernel 内核文件后面跟参数 ro 读取 root=根分区位置  
>
>   initrd 把不常用的驱动，功能编译成模块，需要的时候动态加载
>

![1571160014466](http://imgset.gitee.io/img/1571160014466.png)
